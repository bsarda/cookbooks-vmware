# Add a vmk manually and tag

```shell
esxcfg-vswitch -A Mgmt-DMZ vSwitch0
esxcli network ip interface add -p Mgmt-DMZ
esxcli network ip interface ipv4 set -i vmk1 -I 192.168.4.133 -N 255.255.255.192 -t static
esxcli network ip interface tag add -i vmk1 -t Management
```
