# Settings for cloning properly a nested ESXi

## for memory optimization

```shell
esxcli system settings advanced set -o /Mem/ShareForceSalting -i 0
esxcli system settings advanced set -o /Mem/AllocGuestLargePage -i 1
esxcli system settings advanced set -o /LPage/LPageAlwaysTryForNPT -i 0
```

## for vSAN

```shell
esxcli system settings advanced set -o /VSAN/SwapThickProvisionDisabled -i 1
```

## for network

```shell
esxcli system settings advanced set -o /Net/FollowHardwareMac -i 1
```

## remove warning messages for SSH and shell

```shell
vim-cmd hostsvc/advopt/update UserVars.SuppressShellWarning long 1
```

## to remove any uuid

```shell
sed -i 's#/system/uuid.*##' /etc/vmware/esx.conf
```

Remove local datastores before cloning.

## Save changes

To ensure the file is persisted, run `/sbin/auto-backup.sh`

## Extras: transform to OVA

https://download3.vmware.com/software/vmw-tools/nested-esxi/Nested_ESXi6.7u2_Appliance_Template_v1.ova

Hostname    guestinfo.hostname      Networking
IP Address	guestinfo.ipaddress     Networking
Netmask	    guestinfo.netmask	    Networking
Gateway	    guestinfo.gateway	    Networking
DNS Server	guestinfo.dns           Networking
DNS Domain	guestinfo.domain	    Networking
AD Domain	guestinfo.ad_domain	    Active Directory
AD Username	guestinfo.ad_username	Active Directory
AD Password	guestinfo.ad_password	Active Directory
