# NSX-T clear not realized state

If NSX-T have not realized what you were asking for (due to Policy API and intent-based)

Do a **POST** to `https://{{nsxmgr}}/policy/api/v1/troubleshooting/infra/tree/realization?action=cleanup`

The body is something like:

```json
{
   “paths”: [
      “/infra/realized-state/enforcement-points/default/qos-profiles/virtualwire-402_qos_map1_dvs-2098-QoSProfile/alarms/caaef6a5-94cb-0ab7-5671-4a3e72fd1c00"
    ]
}
```

You can do a **GET** on `https://{{nsxmgr}}/policy/api/v1/troubleshooting/infra?action=list_orphaned_objects&enforcement_point_path=/infra/sites/default/enforcement-points/default`

to get the orphaned objects.

And to delete orphaned objects:

Do a **POST** to https://{{policy}}/policy/api/v1/troubleshooting/infra?action=delete_orphaned_objects&enforcement_point_path=/infra/sites/default/enforcement-points/default

with the orphaned objects in the body:

```json
{
    “orphaned_objects”: [
        {
            “id”: “7679cdc5-ee98-4da4-beaf-49c0aea6daea”,
            “object_type”: “PrincipalIdentity”
        },
        {
            “id”: “0b4b6551-3420-43fd-ada4-5ae211be0e71”,
            “object_type”: “GiServiceProfile”
        }
    ]
}
```
