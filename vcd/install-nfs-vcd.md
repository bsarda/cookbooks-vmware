# Install NFS for vCD

tested on centos 7.x

## Open firewall

```sh
firewall-cmd --permanent --zone=public --add-service=nfs
firewall-cmd --reload
```

## install prereqs

```shell
yum -y install nfs-utils
```

## setup the NFS share

enable service

```shell
systemctl enable nfs-server.service
systemctl start nfs-server.service
```

create folder

```shell
mkdir /transfer
```

assign permissions - NOT for vCD

```shell
chown nfsnobody:nfsnobody /transfer
chmod 755 /transfer
```

add the folder to the exports list, with IP of vCD cells (or the range)

```shell
nano /etc/exports
```

add lines like that:  
10.3.0.32, .33 and .34 are my vcd cells 

```shell
/transfer        10.3.0.32(rw,sync,no_subtree_check)
/transfer        10.3.0.33(rw,sync,no_subtree_check)
/transfer        10.3.0.34(rw,sync,no_subtree_check)
```

refresh exports

```shell
exportfs -a
```

## test on client

where 10.3.0.36 is the ip of my nfs server here

```shell
mkdir -p /mnt/nfs/transfer
mount 10.3.0.36:/transfer /mnt/nfs/transfer
```

