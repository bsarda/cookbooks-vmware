# Install RabbitMQ

validated on a centos 7.x  
Clustered RabbitMQ.

## install erlang

```shell
sudo yum install epel-release
wget https://packages.erlang-solutions.com/erlang-solutions-2.0-1.noarch.rpm
rpm -Uvh erlang-solutions-2.0-1.noarch.rpm
sudo yum install erlang
```

## Install rmq

```shell
yum install rabbitmq-server
chkconfig rabbitmq-server on
service rabbitmq-server start
rabbitmqctl status
rabbitmq-plugins enable rabbitmq_management
```

Try to open the management:  
http://rmq_fqdn/15672  
Default username/password is guest guest  

## open firewall ports

### open fw node1

```shell
sudo iptables -I INPUT -p tcp --dport 4369 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 25672 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 5672 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 15672 --syn -j ACCEPT
```

### open fw node2

```shell
sudo iptables -I INPUT -p tcp --dport 4369 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 25672 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 5672 --syn -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 15672 --syn -j ACCEPT
```


## create the cluster

10.3.0.38 is my first node, 10.3.0.39 is the second node.
md5sum /var/lib/rabbitmq/.erlang.cookie

### cluster prereq on node2

on the node 2:

```shell
rabbitmqctl stop_app
rabbitmqctl reset
service rabbitmq-server stop
cd /var/lib/rabbitmq/
mv .erlang.cookie .erlang.cookie.OLD
scp 10.3.0.38:/var/lib/rabbitmq/.erlang.cookie /var/lib/rabbitmq/
```

### verifie erlang cookie

check if the cookie is the same. On both nodes, pass the command and compare output:

```shell
md5sum /var/lib/rabbitmq/.erlang.cookie
```

### join the cluster

on the node 2:

```shell
chown rabbitmq:rabbitmq .erlang.cookie
service rabbitmq-server start
rabbitmqctl stop_app
rabbitmqctl join_cluster --ram rabbit@rmq1.corp.local
rabbitmqctl cluster_status
```

### start rmq on node2

Then start rabbit mq and check

```shell
rabbitmqctl start_app
rabbitmqctl set_policy ha-all "" '{"ha-mode":"all","ha-sync-mode":"automatic"}'
rabbitmqctl cluster_status
```

### add an admin user

```shell
rabbitmqctl add_user admin vmware
rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
rabbitmqctl set_user_tags admin administrator
```

