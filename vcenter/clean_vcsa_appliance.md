# Clean VCSA

Works on 6.7 versions (tested up to 6.7u3)

## remove log files

```shell
find /storage/log/vmware/ -type f -regex '.*\.log\.[0-9]$' -delete
find /storage/log/vmware/ -type f -name "*.gz" -delete
```

## clean Update Manager repos

```shell
service-control --stop vmware-updatemgr
/usr/lib/vmware-updatemgr/bin/updatemgr-utility.py reset-db
rm -rf /storage/updatemgr/patch-store/*
service-control --start vmware-updatemgr
```

## Remove the db dumps aged more than 15 days

```shell
find /storage/archive/vpostgres -type f -mtime +15 -exec rm -f {} \;
```
