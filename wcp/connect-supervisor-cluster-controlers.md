# Connect to supervisor cluster controlers

tested with vSphere 7.0.

## logs

The logs are in the vCenter Server, in `/var/log/vmware/wcp`

## Get the password

SSH to the vCenter Server, enter in shell mode.

```sh
shell
```

launch the decryption util

```console
root@vcsa [ ~ ]# /usr/lib/vmware-wcp/decryptK8Pwd.py
Read key from file

Connected to PSQL

Cluster: domain-c8:bd5beffb-20e3-41d3-8ec5-7732505838ca
IP: 10.4.1.10
PWD: X2AMDYuaxLxigGSbaoi2b6J5k/8BMYeR0XC/zYR5SINEWzHHru0vqh8CV6H+Vby3j5gUoUe31dbyf08IXTHkS0qmoTV9/RN2J3/+jugVkSU6OJXV30yMWpu6uGRhsWaIy+um54Mjn2puLjMQ5blaiTkBgIdcC7gWMHO0SGW5A1c=
------------------------------------------------------------
```

## Connect

connect to the machine
IP is one of the IPs of the supervisor vms.  

root and the password is above
