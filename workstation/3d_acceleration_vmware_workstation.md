# 3D ACCELERATION DANS WORKSTATION

install the mesa utils:

```shell
sudo apt install mesa-utils
```

ensure we have the drivers

```shell
glxinfo | grep direct
glxgears
```

add all the drivers and not only the whitelist

```shell
nano ~/.vmware/preferences
```

add/modify the line

```shell
mks.gl.allowBlacklistedDrivers = "TRUE"
```

