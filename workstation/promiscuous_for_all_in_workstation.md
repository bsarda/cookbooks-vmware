# PROMISCUOUS FOR ALL IN WORKSTATION

Source:
[https://bbs.archlinux.org/viewtopic.php?id=65508]

## Temporary

Add the rights, at minimum the user's permissions on networks

```shell
sudo chmod a+rw /dev/vmnet*
```

## Temporary clean

change the owner group:

```shell
sudo groupadd vmw-promiscuous
sudo usermod -a -G vmw-promiscuous bsarda
sudo usermod -a -G vmw-promiscuous root
sudo chgrp vmw-promiscuous /dev/vmnet*
sudo chmod g+rw /dev/vmnet*
```

## Persistant

```shell
sudo vi /etc/init.d/vmware
```

find the function vmwareStartVmnet, adding the lines chgrp and chmod like this:

```shell
vmwareStartVmnet() {
  vmwareLoadModule $vnet
  "$BINDIR"/vmware-networks --start >> $VNETLIB_LOG 2>&1
  chgrp promiscuous /dev/vmnet*
  chmod g+rw /dev/vmnet*
}
```

it could be only `chmod a+rw /dev/vmnet*` if the station is not shared.
